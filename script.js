let invalidSelectorsMessage = 'Были удалены некорректные селекторы и дубликаты.',
    hasInvalidSelectors = false;

/**
 * Функция позволяет получить список селекторов из HTML элемента textarea
 * @param data - Принимает на вход HTML элемент <textarea>
 * @returns [*] - Возвращает массив селекторов
 */
function getSelectorsList(data) {
    let selectorsArray = data.value.split('\n');
    selectorsArray = selectorsArray.filter((element, index) => {
        return selectorsArray.indexOf(element) === index;
    });
    // TODO: Выяснить почему не работает splice как надо. Временное решение
    let resultArray = [];
    for (let el in selectorsArray) {
        let isSelectorValidated = validateSelector(selectorsArray[el]);
        if (isSelectorValidated === false) {
            selectorsArray.splice(parseInt(el), 1, 'DELETE'); // Помечаем элемент массива для удаления
            hasInvalidSelectors = true;
        }
        resultArray = selectorsArray.filter(function(f) { return f !== 'DELETE' }) // Удаляем помеченный элемент
    }
    return resultArray;
}

/**
 * Функция позволяет валидировать селектор.
 * @param selector - Строка содержащая селектор
 * @returns {boolean} - Возвращает true если селектор рабочий, false - если нет.
 */
function validateSelector(selector) {
    try {
        document.querySelector(selector);
        return true;
    }
    catch(e) {

    }
    return false;
}

/**
 * Показать ошибку в alert()
 */
function showError() {
    alert('Введены неправильные данные!');
}

/**
 * Функция проверяет являются ли входные данные числом, а также проверяет на соответствие значениям
 * @param data - Строка содержащая число
 * @returns {boolean} - Возвращает true - если строка является валидным числом, false - если иное значение.
 */
function checkNumericData(data) {
    if (data === null || data === 0 || typeof data !== "number" || data < 0 || isNaN(data))
    {
        return false;
    } else return true;
}

/**
 * Позволяет протестировать скорость поиска селекторов.
 * @param selectors - Принимает на вход массив селекторов
 * @param toTable - Если true - выводит таблицу с результатами в консоль.
 * @param numberOfElements - Количество создаваемых элементов
 * @param numberOfIterations - Количество итераций цикла
 */
function testSpeed(selectors, toTable = false, numberOfElements = 5000, numberOfIterations = 100){
    let html, t0, t1, a, sum,
        result = [];
    for(let s = 0, length = selectors.length; s < length; s++){
        a = t0 = t1 = null;
        sum = 0;
        html = ''

        for(let n = numberOfElements; --n !== 0;){
            html += '<div class="test" id="test' + n + '" data-test=""></div>';
        }

        document.body.innerHTML = html;

        for(let i = numberOfIterations; --i !== 0;){
            t0 = performance.now();
            a = document.querySelectorAll(selectors[s]);
            t1 = performance.now();
            sum += (t1 - t0);
        }

        result.push({
            time: sum / numberOfIterations,
            selector: selectors[s]
        });

    }

    result.sort(function(a, b){
        return a.time - b.time;
    });
    let htmlResult = '';
    if (hasInvalidSelectors === true) {
        htmlResult = `<div class="result"><h2>Результаты измерения:<br/><h4 class="warning-text">${invalidSelectorsMessage}</h4></h2>`;
    } else {
        htmlResult = '<div class="result"><h2>Результаты измерения:<br/></h2>';
    }
    for (let i = 0; i < result.length; i++)
    {
        htmlResult += `<div class="result-box"><div class="result-time">${result[i].time}</div><div class="result-selector">${result[i].selector}</div></div>`;
    }
    htmlResult += '</div>';
    document.body.innerHTML += htmlResult;
    document.body.innerHTML += `<button class="result-box__Btn" id="resultBox__refreshBtn" type="submit">Еще раз</button>`;
    document.getElementById('resultBox__refreshBtn').addEventListener("click",
        function (event) {
           window.location.reload();
        });
    if (toTable === true) {
        console.table(result, ['time', 'selector']);
    }
}

document.getElementById('config-box__speedBtn').addEventListener("click",
    function (event) {
        let numberOfElements = document.getElementById('config-box_elemInput').value;
        if (checkNumericData(parseInt(numberOfElements)) === false) {
            showError();
            return;
        }
        let numberOfIterations = document.getElementById('config-box_iterInput').value;
        if (checkNumericData(parseInt(numberOfIterations)) === false) {
            showError();
            return;
        }
        let textArea = document.getElementById('config-box__textArea');
        let textAreaVal = textArea.value.trim();
        if (!textAreaVal.length || textAreaVal === '' || textAreaVal === ' ' || !isNaN(textAreaVal))
        {
            showError();
            return;
        } else {
            let selectorsList = getSelectorsList(textArea);
            console.log(selectorsList)
            if (!selectorsList.length) {
                showError();
                return;
            } else testSpeed(selectorsList, false, numberOfElements, numberOfIterations);
        }
});